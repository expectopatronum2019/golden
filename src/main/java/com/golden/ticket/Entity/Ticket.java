package com.golden.ticket.Entity;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity

@Table(name = "ticket")
public class Ticket {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idTicket;
	
	@Column(name = "totalAmount")
	private Integer totalAmount;
	
	@Column(name = "detaCreation")
	private Timestamp creationDate;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "ticket")
	private List<Detail> details;

	public Ticket() {
	}
	
	public Ticket(Integer idTicket, Integer totalAmount, Timestamp creationDate, List<Detail> details) {
		super();
		this.idTicket = idTicket;
		this.totalAmount = totalAmount;
		this.creationDate = creationDate;
		this.details = details;
	}

	public Integer getIdTicket() {
		return idTicket;
	}

	public void setIdTicket(Integer idTicket) {
		this.idTicket = idTicket;
	}

	public Integer getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
}
