package com.golden.ticket.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity

@Table(name = "Detail")
public class Detail {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idDetail;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "amount")
	private Float amount;

	@ManyToOne
	@JoinColumn(name = "FK_TICKET",nullable = false)
	private Ticket ticket;
	
	public Detail() {
	}
	
	public Detail(Integer idDetail, String description, Float amount) {
		super();
		this.idDetail = idDetail;
		this.description = description;
		this.amount = amount;
	}

	public Integer getIdDetail() {
		return idDetail;
	}

	public void setIdDetail(Integer idDetail) {
		this.idDetail = idDetail;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}
}
